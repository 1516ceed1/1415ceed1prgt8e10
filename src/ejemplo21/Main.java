/*
 * Main.java
 *
 * Created on 24 de agosto de 2008, 12:29
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package ejemplo21;
import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {
    
    /** Creates a new instance of Main */
    public Main() {
    }
    
    /**
     * @param args the command line arguments
     */
     public static void main(String[] args) {
        // TODO code application logic here
        Connection conn = null;
        ResultSetMetaData rsmd = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            //Registrando el Driver
            String driver = "com.mysql.jdbc.Driver";
            Class.forName(driver).newInstance();
            System.out.println("Driver "+driver+" Registrado correctamente");
            
            //Abrir la conexi�n con la Base de Datos
            System.out.println("Conectando con la Base de datos...");
            String jdbcUrl = "jdbc:mysql://localhost:3306/empresa";
            conn = DriverManager.getConnection(jdbcUrl,"root","");
            System.out.println("Conexi�n establecida con la Base de datos...");
            
            //Creamos el objeto statement y obtenemos el ResultSetMetaData
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from sucursales");
            rsmd = rs.getMetaData();
            
            infoColumnas(rsmd);
        } catch(SQLException se) {
            //Errores de JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Errores de Class.forName
            e.printStackTrace();
        } finally {
            try {
                if(conn!=null)
                    conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try  
    }
    
     public static void infoColumnas(ResultSetMetaData rsmd) throws SQLException {
         System.out.println(">>Consultado la tabla ["+rsmd.getTableName(1)+"]");
         int numCols = rsmd.getColumnCount();
         System.out.println("Numero de columnas: "+numCols);
         for (int col=1; col <= numCols; col++) {
            System.out.println("Nombre: "+ rsmd.getColumnName(col)+" Tipo: " + rsmd.getColumnTypeName(col));
         }
     }
     
}
