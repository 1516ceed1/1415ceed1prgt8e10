/*
 * Main.java
 *
 * Created on 29 de agosto de 2008, 23:20
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package ejemplo23MysqlDataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {

  /**
   * Creates a new instance of Main
   */
  public Main() {
  }

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    Connection conn = null;
    try {
      //Instanciamos el objeto DataSource
      MysqlDataSource mds = new MysqlDataSource();

      //Inicializamos las propiedades
      mds.setUser("paco");
      mds.setPassword("");
      mds.setDatabaseName("empresa");
      mds.setPortNumber(3306);
      mds.setServerName("localhost");

      //Abrimos la conexi�n
      conn = mds.getConnection();
      System.out.println("Conexi�n establecida!");


    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores de Class.forName
      e.printStackTrace();
    } finally {
      try {
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try  

  }
}
