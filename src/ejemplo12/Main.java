/*
 * Main.java
 *
 * Created on 13 de agosto de 2008, 12:23
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */
package ejemplo12;

import java.sql.*;

/**
 *
 * @author alberto
 */
public class Main {

  /**
   * Creates a new instance of Main
   */
  public Main() {
  }

  public static void printRow(ResultSet rs) throws SQLException {
    //variables
    int id;
    String serie;
    int numero;
    java.util.Date fecha;
    String cliente;
    String vendedor;

    //Obtenemos la informacin por el nombre de la columna

    //obtener_la_informacion_del_resultset;


    //Mostramos la informacin
    System.out.print("Numero de Fila=" + rs.getRow());
    System.out.print(", id: " + id);
    System.out.print(", serie: " + serie);
    System.out.print(", numero: " + numero);
    System.out.print(", fecha: " + fecha);
    System.out.println(", cliente: " + cliente);
    System.out.println(", vendedor: " + vendedor);
  }//end printRow()

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    // TODO code application logic here
    Connection conn = null;
    Statement stmt = null;
    ResultSet rs = null;

    try {
      //Registrando el Driver

      //registramos_el_driver;

      System.out.println("Driver " + driver + " Registrado correctamente");

      //Abrir la conexi�n con la Base de Datos
      System.out.println("Conectando con la Base de datos...");

      //obtenemos_la_conexion;

      System.out.println("Conexion establecida con la Base de datos...");

      //crear_la_sentencia_en_modo_scrollable_y_de_solo_lectura;

      //Ejecutamos la SELECT sobre la tabla articulos
      String sql = "select facturas.id, serie, numero, fecha,  clientes.nombre, vendedores.nombre from facturas, clientes, vendedores where facturas.cliente=clientes.id and facturas.vendedor=vendedores.id";

      //ejecutar_la_query;

      System.out.println("Situamos el cursor al final");

      //situar_cursor_despues_del_ultimo_registro;


      while (recorrer_en_orden_inverso_el_resultset) {
        printRow(rs);
      }

      //Accedemos a la �ltima
      accedemos_a_la_ultima_fila;
      printRow(rs);

      //Accedemos a la primera
      accedemos_a_la_primera_fila;
      printRow(rs);

      //Accedemos a la cuarta
      accedemos_a_la_cuarta_fila;
      printRow(rs);

      //Accedemos a la tercera
      accedemos_a_la_tercera_fila;
      printRow(rs);


    } catch (SQLException se) {
      //Errores de JDBC
      se.printStackTrace();
    } catch (Exception e) {
      //Errores de Class.forName
      e.printStackTrace();
    } finally {
      try {
        if (rs != null) {
          rs.close();
        }
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException se) {
        se.printStackTrace();
      }//end finally try
    }//end try
  }
}
