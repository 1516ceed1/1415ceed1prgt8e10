/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @date 09-mar-2015 Fichero Insert.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Insert {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        // TODO code application logic here
        String driver = "com.mysql.jdbc.Driver";
        Class.forName(driver).newInstance();
        String jdbcUrl = "jdbc:mysql://127.0.0.1:3306/ceedprgt8";
        Connection con = DriverManager.getConnection(jdbcUrl, "alumno", "alumno");

        Statement st = con.createStatement();
        String sql = "insert into alumnos (nombre,edad,email) values ('Pepe',12,'prueba@a.es');";
        System.out.println(sql);
        st.executeUpdate(sql);
        con.close();

    }
}
